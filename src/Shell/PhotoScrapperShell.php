<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\Network\Http\Client;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Instagram;

/** 
The console utility that does the following :
    1. Grab a list of locations from foursquare and store in the db.
    2. Loop through each of these locations and query instagram.
    3. Store all matching instagram photos in the db.
**/
class PhotoScrapperShell extends Shell
{
	protected $http;

	function __construct(){
		parent::__construct();

		// setup some basic error styling and off course of Http client to make requests.
		$this->http = new Client();
		$this->_io->styles('error', ['text' => 'red', 'blink' => true]);
		$this->_io->styles('success', ['text' => 'green', 'blink' => true]);
	}

	// simple method to take an array of errors and display them a bit neater than print_r

	private function printFormattedErrors($errors){
		foreach($errors as $errorList){
			foreach($errorList as $key=>$value){
				$e = sprintf("<error>%s : %s</error>", $key, $value);
				$this->out($e);
		    }
		}
	}

	private function pollFoursquare(){
		$config = Configure::read("Foursquare");
                
		//setup endpoint url using foursquare settings in our app settings file
		$endpoint = sprintf(
			"%s/venues/suggestcompletion?radius=30&client_id=%s&client_secret=%s&v=20130815&near=%s&query=%s",
			$config['base_endpoint'], 
		    $config['client_id'], 
		    $config['client_secret'],
		    $config['city'],
		    $config['searchfor']
		);

		$response = $this->http->get($endpoint);

		// 200 - succesfully response , we have data so parse it.
		if($response->code == 200){
			$response = json_decode($response->body);
			$locationsTable = TableRegistry::get("Locations");
			$user_id = 1;
			foreach($response->response->minivenues as $location){
					$lat = $location->location->lat;
					$lng = $location->location->lng;

					//make sure we not duplicating data.
					if($locationsTable->checkDuplicateLocation($lat, $lng, $user_id) == 0){
						
						// Instatiate and a new row in our database table 
						$locationEntity = $locationsTable->newEntity();
					    $locationEntity->latitude = $lat;
						$locationEntity->longitude = $lng;
						$locationEntity->name = $location->name;
						$locationEntity->user_id = 1;
						$locationsTable->save($locationEntity);
						$errors = $locationEntity->errors();
						if(count($errors) > 0){
							$this->printFormattedErrors($errors);
						}else{
							$msg = sprintf("<success>Successfully added location:%s,%s, %s</success> \n", 
					    	    $location->name, $lat, $lng);
							$this->out($msg);
					    }
				    }else{
				    	$error = sprintf("<error>Location already exists:%s,%s, %s</error> \n", 
				    	    $location->name, $lat, $lng);
				    
				    	$this->out($error);
				    }
	
			}

		}

	}

	private function pollInstagram(){
		$instagram = new Instagram();
		// run our API test , if it fails - prompt the user to generate a new token
		// by visiting the authorization URL. 
		if($instagram->testApi() == false){

			$this->out("<error>Access Token invalid - please visit the below link in your browser and press enter once complete :\n</error>");
			$this->in($instagram->generateCodeURL());

		}
		$accessToken = $instagram->getAccessToken();

		$locationsTable = TableRegistry::get("Locations");
		$locations = $locationsTable->find();
		$locations->select(['latitude', 'longitude', 'name']);
		
		/**
		order by ID so we only process the newest 200 locations , this 
		is a very simplistic aproach , for a more robust system - i would
		utilize a task queue similar to python's celery and also keep track
		off which locations have already been synced before.

		**/
		$locations->order(['id' => "DESC"]);
		$locations->limit(200);
		$instagramPhotosTable = TableRegistry::get("instagramPhotos");

		// loop through each location in our database and do a search for images on instagram.
		foreach($locations as $location){
			$this->out(sprintf("Processing Images for location: %s, Latitude: %s, Longitude: %s",
				$location->name, $location->longitude, $location->latitude));
			$media = $instagram->getMedia($location->latitude, $location->longitude, $accessToken);
			foreach($media->data as $mediaItem){
				if($mediaItem->type == "image"){
					$InstagramPhoto = $instagramPhotosTable->newEntity([
						"link" => $mediaItem->link,
						"caption" => isset($mediaItem->caption->text) ? $mediaItem->caption->text : null,
						"image_url" => $mediaItem->images->standard_resolution->url
					]);

					if(!$InstagramPhoto->errors()){
						$instagramPhotosTable->save($InstagramPhoto);
					}else{
						$this->printFormattedErrors($InstagramPhoto->errors());
					}
					
			}

		}
	}
   }

    public function main()
    {
    	$this->pollFoursquare();
    	$this->pollInstagram();
    	
    }
}
