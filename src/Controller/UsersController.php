<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Http\Client;
use App\Model\Entity\Instagram;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    private function findImages($city, $q){
        $http = new Client();
        $instagram = new Instagram();
        $media = [];
        $accessToken = null;

        if ($instagram->testApi() == false){
          $session = $this->request->session();
          $session->write ("redirect_uri" , "/Users/search");
          return ['error' => true , "redirect" => $instagram->generateCodeURL()];
         } else{
	    $accessToken = $instagram->getAccessToken();
        }

        $config = Configure::read("Foursquare");
        $endpoint = sprintf(
            "%s/venues/suggestcompletion?radius=5&client_id=%s&client_secret=%s&v=20130815&near=%s&query=%s",
	    $config['base_endpoint'], 
            $config['client_id'], 
            $config['client_secret'],
            $city,
            $q
         );
         $response = $http->get($endpoint);
         if($response->code == 200){
             $response = json_decode($response->body);
             foreach($response->response->minivenues as $location){
                 $lat = $location->location->lat;
                 $lng = $location->location->lng;
                 $name = $location->name;
                 $accessToken = $instagram->getAccessToken();
                 $media_data = $instagram->getMedia($lat, $lng, $accessToken);
                 foreach($media_data->data as $mediaItem){
                     if($mediaItem->type == "image"){
                         $media[] = [$mediaItem->link, isset($mediaItem->caption->text) ? $mediaItem->caption->text : "" ,
                                     $mediaItem->images->standard_resolution->url];
                     }
                 }
             }

         }
        return $media;
    }
    public function search(){
	if($this->request->is(['patch', 'post', 'put'])){
              $images = $this->findImages($this->request->data['city'], $this->request->data['searchfor']);
              if(!isset($images['error'])){
                 $this->set("images", $images);
                 $this->set("page_title", "Instagram Photo Search Results");
                 $this->render('/Users/search_results');
              }else{
                  $this->redirect($images['redirect_uri']);
              }

        }
      $this->set("page_title", "Instagram Photo Search");

    }
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Locations']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
