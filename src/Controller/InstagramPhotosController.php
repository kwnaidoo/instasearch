<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * InstagramPhotos Controller
 *
 * @property \App\Model\Table\InstagramPhotosTable $InstagramPhotos
 */
class InstagramPhotosController extends AppController
{

    public function index()
    {
        $instagramPhotos = $this->paginate($this->InstagramPhotos);

        $this->set(compact('instagramPhotos'));
        $this->set('_serialize', ['instagramPhotos']);
    }

    public function view($id = null)
    {
        $instagramPhoto = $this->InstagramPhotos->get($id);

        $this->set('instagramPhoto', $instagramPhoto);
        $this->set('_serialize', ['instagramPhoto']);
    }


}
