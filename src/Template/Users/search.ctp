<div class="users view large-12 columns content">

<h2> Photo Search</h2>
<div>

<form method="post" action="/Users/search" onsubmit="return validate();">

<label for="city">City : </label> <input type="text" name="city" required id="city" /><br />
<label for="searchfor"> Search For? : </label> <input type="text" name="searchfor" required  id="searchfor" /> <br />

<input type="submit" name="search" value="Search"/>

</form>
</div>
</div>

<script>
function validate(){
  city = document.getElementById("city");
  searchfor = document.getElementById("searchfor");
  errors = null;
  
  if(city.value.length < 3){
    errors = "Please enter a valid city name";
  }
  if(searchfor.value.length < 3){

    errors = "\nPlease enter a valid searchfor term";
  }
  if(errors == null){
   return true;
  }else{
   alert(errors);
   return false;
  }
}

</script>
