<div class="users view large-12 columns content">
<h2> Photo search results</h2>
<?php if(count($images) > 0) :?>
<table>
<?php
$i = 0;
?>

<?php foreach($images as $img):?>
<?php if($i==0):?>
<tr>
<?php endif;?>
<td> <a href="<?php print $img[0];?>" target="_blank"><img src="<?php print $img[2];?>" class="thumb" /> </a><br /> 

<?php if($img[1]):?> 
    <b> <?php print substr($img[1], 0, 20);?></b>
<?php endif;?>

</td>
<?php if($i == 3):?>
</tr>
<?php $i=0;?>
<?php endif;?>
<?php $i+=1;?>

<?php endforeach;?>
<?php if($i > 1 && $i < 3):?>
    </tr>
<?php endif;?>
</table>
<?php else:?>
<b> Sorry no images found . Click <a href="/Users/search"> here </a> to try again.</b>
<?php endif;?>
</div>

