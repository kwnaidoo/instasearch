<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;


class InstagramPhoto extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
