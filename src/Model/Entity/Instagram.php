<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Core\Configure;
use Cake\Network\Http\Client;
use Cake\ORM\TableRegistry;

/** A Non database entity that will communicate with the 
    Instagram API and retrieve data accordingly 

**/

class Instagram extends Entity
{
    var $http; // http client object to make GET / POST / PUT etc.. requests.
    var $code; // required by instagram to generate a token

    public function __construct(){
        parent::__construct();
        $this->http = new Client();
    }

    // convenience method to make reading Instagram related settings easier.
    private function getSetting($key){
        return Configure::read('Instagram')[$key];
    }

    /**
     This will return an authorization URL which when visited
     in the browser will complete a series of steps to 
     authenticate the current instagram user.
    **/
    public function generateCodeURL(){
        return sprintf(
            "%s?client_id=%s&redirect_uri=%s&response_type=code&scope=public_content",
            $this->getSetting("authorize_url"),
            $this->getSetting("client_id"),
            $this->getSetting("callback_url")
        );
    }

    // Generate and store an access token in our settings table
    public function genAccessToken(){
        //setup required post variables as per Instagram's requirements
        $payload = [
          'client_id' => $this->getSetting("client_id"),
          'client_secret' => $this->getSetting('client_secret'),
          'grant_type' => 'authorization_code',
          'redirect_uri' => $this->getSetting('callback_url'),
          'code' => $this->code

        ];
        //Post the above to the URL endpoint as stored in app settings.
        $response = $this->http->post($this->getSetting('token_url'), $payload);
        
        // The returned data is raw json text so parse it into a PHP object.

        $response = json_decode($response->body);

        if(isset($response->error_message)){
            return ['error' => $response->error_message , 'access_token' => False];
        }else{
            $accessToken = $response->access_token;
            // If we got a token back than update the token settings in our db.
            $settingsTable = TableRegistry::get("Settings");
            $query = $settingsTable->query();
            $query->update()
                ->set(['setting_value' => $accessToken])
                ->where(['setting_key' => 'instagram_access_token'])
                ->execute();
                return $response;
        }

    }
    
    //simply reads the access_token value in our db and returns it.
    public function getAccessToken(){
        $settingsTable = TableRegistry::get("Settings");
        $setting = $settingsTable->find()->where(["setting_key" => "instagram_access_token"])->first();
        return $setting['setting_value'];
    }

    /** 
        a sort of "PING" method that will ensure that the API is authenticating
        fine , the calling method can than regenerate an access_token should this
        fail. 
    **/

    public function testApi(){
        $testLink = $this->getSetting("test_url");
        $accessToken = $this->getAccessToken();
        $testLink = sprintf("%s?access_token=%s", $testLink, $accessToken);
        $response = $this->http->get($testLink);
        $body = json_decode($response->body);
        if(isset($body->meta->error_message)){
            return false;
        }else{
            return true;
        }
    }

    /**
    Takes in the coordinates & access token in order
    to perform a search for all media items matching that
    geo radius.
    **/
    public function getMedia($lat, $lng, $accessToken){
      $search = sprintf(
          "%s?lat=%s&lng=%s&access_token=%s&distance=100",
          $this->getSetting("media_search_url"),  
          $lat, $lng, $accessToken);

          $response = $this->http->get($search);
          return json_decode($response->body); 

    }

}
