<?php
namespace App\Model\Table;

use App\Model\Entity\InstagramPhoto;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InstagramPhotos Model
 *
 */
class InstagramPhotosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('instagram_photos');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('link', 'create')
            ->notEmpty('link');

        $validator
            ->requirePresence('image_url', 'create')
            ->notEmpty('image_url');

        $validator->add('image_url', 'Image URL invalid', [
            'rule' => 'validateUnique',
            'provider' => 'table',
            'message' => "Image URL already exists"
        ]);
        $validator->add('link', 'Link Invalid', [
            'rule' => 'validateUnique',
            'provider' => 'table',
            'message' => "Link already exists"
        ]);

        return $validator;
    }
}
