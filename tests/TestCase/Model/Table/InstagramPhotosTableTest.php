<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstagramPhotosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstagramPhotosTable Test Case
 */
class InstagramPhotosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstagramPhotosTable
     */
    public $InstagramPhotos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instagram_photos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstagramPhotos') ? [] : ['className' => 'App\Model\Table\InstagramPhotosTable'];
        $this->InstagramPhotos = TableRegistry::get('InstagramPhotos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstagramPhotos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
