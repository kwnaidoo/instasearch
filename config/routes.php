<?php

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;


Router::defaultRouteClass('DashedRoute');

Router::scope('/', function (RouteBuilder $routes) {

    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /** Custom routes added :

    /callback - will do the Instagram authorization to store an access token
    /api - simple API to get photo information
    **/
    $routes->connect('/callback/', ['controller' => 'Pages', 'action' => 'authorizeInstagram']);
    $routes->extensions(['json']);
    $routes->resources('InstagramPhotos');


    $routes->fallbacks('DashedRoute');
});


Plugin::routes();
