<?php

use Phinx\Migration\AbstractMigration;

class InstagramPhotos extends AbstractMigration
{
  
    public function up()
    {
        $this->table('instagram_photos')
            -> addColumn("link", 'string', ['limit' => 255])
            -> addColumn('caption', 'text', ['null' => true ])
            -> addColumn('image_url', 'string', ['limit' => 255])
            -> save();

    }
}
