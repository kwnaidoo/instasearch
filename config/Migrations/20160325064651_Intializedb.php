<?php
use Migrations\AbstractMigration;

class Intializedb extends AbstractMigration
{
   
    public function up()
    {
        // store decimal options in one place - neater to reuse
        $decimal_opts = ['precision' => 18, 'scale' => 15];
        $this->table("users")
            -> addColumn("username", 'string', ['limit' => 50])
            -> addColumn('password', 'string', ['limit' => 100])
            -> addColumn('email', 'string', ['limit' => 255])
            -> save();

        $this->table("locations")
            -> addColumn("name", 'string', ['limit' => 50])
            -> addColumn('user_id', 'integer')
            -> addColumn('latitude', 'decimal', $decimal_opts)
            -> addColumn('longitude', 'decimal', $decimal_opts)
            -> save();




    }

    public function down()
    {
        $this->dropTable("locations");
    }
}
