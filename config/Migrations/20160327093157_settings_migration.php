<?php

use Phinx\Migration\AbstractMigration;

class SettingsMigration extends AbstractMigration
{
    public function up()
    {
        // store decimal options in one place - neater to reuse
        $decimal_opts = ['precision' => 18, 'scale' => 15];
        /** 
         Settings app will store common dynamic settings
        **/

        $this->table('settings')
            -> addColumn("setting_key", 'string', ['limit' => 50])
            -> addColumn('setting_value', 'string', ['limit' => 255])
            -> save();


    }

    public function down()
    {
        $this->dropTable('settings');

    }
}
