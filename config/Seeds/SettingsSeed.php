<?php
use Phinx\Seed\AbstractSeed;

/**
 * Settings seed.
 */
class SettingsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *737508163.009e319.baaf5f5462d44ccc9a7da7d0ebaf6370
     * @return void
     */
    public function run()
    {
        $data = [];
        $instagram = [
            'setting_key' => 'instagram_access_token',
            'setting_value' => '3077019565.467ede5.15826b609d864b06ac7329dbc8892b28'
        ];
        $data[] = $instagram;

        $table = $this->table('settings');
        $table->insert($data)->save();
    }
}
