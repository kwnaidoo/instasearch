
[36mWelcome to CakePHP v3.2.5 Console[0m
---------------------------------------------------------------
App : src
Path: /home/kevin/cakephp/instasearch/src/
PHP : 5.5.9-1ubuntu4.14
---------------------------------------------------------------
stdClass Object
(
    [meta] => stdClass Object
        (
            [code] => 200
            [requestId] => 56f79ae6498e4653c55c5110
        )

    [response] => stdClass Object
        (
            [minivenues] => Array
                (
                    [0] => stdClass Object
                        (
                            [id] => 530ae78e498eac2f11ca04e8
                            [name] => Coffee Guys
                            [location] => stdClass Object
                                (
                                    [address] => 10 Mount Argus Drive
                                    [city] => Durban North
                                    [state] => KwaZulu-Natal
                                    [postalCode] => 4051
                                    [country] => ZA
                                    [lat] => -29.8579
                                    [lng] => 31.0292
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d16d941735
                                            [name] => Café
                                            [pluralName] => Cafés
                                            [shortName] => Café
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/cafe_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [1] => stdClass Object
                        (
                            [id] => 506d2252e4b0f68b60e3c8a3
                            [name] => House Of Coffees
                            [location] => stdClass Object
                                (
                                    [address] => 319 Smith St
                                    [city] => Durban
                                    [state] => KZN
                                    [postalCode] => 4001
                                    [country] => ZA
                                    [lat] => -29.859608396559
                                    [lng] => 31.023588180542
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [2] => stdClass Object
                        (
                            [id] => 50ea7d01e4b07ff463fdcb92
                            [name] => Nedbank Coffee shop
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.850118489572
                                    [lng] => 31.030158832209
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d16d941735
                                            [name] => Café
                                            [pluralName] => Cafés
                                            [shortName] => Café
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/cafe_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [3] => stdClass Object
                        (
                            [id] => 4bb1ca7df964a5202ea33ce3
                            [name] => Standard Bank head Office coffee shop
                            [location] => stdClass Object
                                (
                                    [address] => NMR Avenue
                                    [city] => Durban
                                    [state] => KwaZulu Natal
                                    [country] => ZA
                                    [lat] => -29.846838225509
                                    [lng] => 31.026037584851
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                    [2] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d10a951735
                                            [name] => Bank
                                            [pluralName] => Banks
                                            [shortName] => Bank
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/shops/financial_
                                                    [suffix] => .png
                                                )

                                        )

                                    [3] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06378d81259
                                            [name] => Shop & Service
                                            [pluralName] => Shops & Services
                                            [shortName] => Shops
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/shops/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [4] => stdClass Object
                        (
                            [id] => 50ca02c1245f2d4aa8c2b7b1
                            [name] => Lobby Lounge Coffee Bar
                            [location] => stdClass Object
                                (
                                    [address] => 12-14 Walnut Road
                                    [crossStreet] => Hilton Durban
                                    [city] => Durban
                                    [state] => NA
                                    [postalCode] => 4001
                                    [country] => ZA
                                    [lat] => -29.84519958
                                    [lng] => 31.0352993
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1c4941735
                                            [name] => Restaurant
                                            [pluralName] => Restaurants
                                            [shortName] => Restaurant
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [5] => stdClass Object
                        (
                            [id] => 4fbf6deae4b09d6bb234bc3d
                            [name] => LoveCoffee
                            [location] => stdClass Object
                                (
                                    [address] => Windermere Road
                                    [city] => Durban
                                    [country] => ZA
                                    [lat] => -29.822072
                                    [lng] => 31.022658
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [6] => stdClass Object
                        (
                            [id] => 4d6653bf7a6fa14368eb7098
                            [name] => The Bean Green Coffee Company
                            [location] => stdClass Object
                                (
                                    [address] => 147 Helen Joseph Rd
                                    [crossStreet] => Bulwer Rd
                                    [city] => Durban
                                    [state] => KwaZulu-Natal
                                    [postalCode] => 4083
                                    [country] => ZA
                                    [lat] => -29.859495857773
                                    [lng] => 30.998501133675
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [7] => stdClass Object
                        (
                            [id] => 540ab105498e0b9b2c13b8f1
                            [name] => Seattle Coffee Co Virginia Circle
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.772545536572
                                    [lng] => 31.052705609601
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [8] => stdClass Object
                        (
                            [id] => 522f2743498e32b79493c979
                            [name] => Coffee Break
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.841449
                                    [lng] => 31.020682
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [9] => stdClass Object
                        (
                            [id] => 5417fd77498e923d194ad91e
                            [name] => Three Monkeys Coffee Bar
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.854694076773
                                    [lng] => 30.997280133078
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d143941735
                                            [name] => Breakfast Spot
                                            [pluralName] => Breakfast Spots
                                            [shortName] => Breakfast
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/breakfast_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [10] => stdClass Object
                        (
                            [id] => 53368fcd498e99a2bd12e021
                            [name] => Coffee Tree
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.860726465583
                                    [lng] => 30.989931465863
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [11] => stdClass Object
                        (
                            [id] => 4caefa4ecbab236aa32a9473
                            [name] => SOHO Clothing And Coffee Shop
                            [location] => stdClass Object
                                (
                                    [city] => Durban
                                    [state] => KwaZulu-Natal
                                    [country] => ZA
                                    [lat] => -29.834239
                                    [lng] => 31.02246263
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1ff941735
                                            [name] => Miscellaneous Shop
                                            [pluralName] => Miscellaneous Shops
                                            [shortName] => Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/shops/default_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06378d81259
                                            [name] => Shop & Service
                                            [pluralName] => Shops & Services
                                            [shortName] => Shops
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/shops/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [12] => stdClass Object
                        (
                            [id] => 50d364d6e4b02a0dbcfebd59
                            [name] => Indian Coffee Shop
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.839432
                                    [lng] => 31.01039
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d10f941735
                                            [name] => Indian Restaurant
                                            [pluralName] => Indian Restaurants
                                            [shortName] => Indian
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/indian_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [13] => stdClass Object
                        (
                            [id] => 4eed9f2a46907303bf42751a
                            [name] => Gloria Jean's Coffee - Davenport
                            [location] => stdClass Object
                                (
                                    [address] => Brand Road
                                    [city] => Durban
                                    [state] => Kwazulu Natal
                                    [postalCode] => 4001
                                    [country] => ZA
                                    [lat] => -29.860759411065
                                    [lng] => 31.000422224366
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d16d941735
                                            [name] => Café
                                            [pluralName] => Cafés
                                            [shortName] => Café
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/cafe_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [14] => stdClass Object
                        (
                            [id] => 4ec1f30602d5a63a4011c312
                            [name] => Fortunes Coffee Shop
                            [location] => stdClass Object
                                (
                                    [city] => Berea
                                    [state] => KwaZulu-Natal
                                    [country] => ZA
                                    [lat] => -29.833099039128
                                    [lng] => 31.02006264627
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1bc941735
                                            [name] => Cupcake Shop
                                            [pluralName] => Cupcake Shops
                                            [shortName] => Cupcakes
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/cupcakes_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                    [2] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1d0941735
                                            [name] => Dessert Shop
                                            [pluralName] => Dessert Shops
                                            [shortName] => Desserts
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/dessert_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [15] => stdClass Object
                        (
                            [id] => 4ef0a164d3e3a2671281842f
                            [name] => Woolworths Coffee shop, Musgrave Centre
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.848159739252
                                    [lng] => 31.000915094032
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [16] => stdClass Object
                        (
                            [id] => 53c8d47c498edc8c0e2b0330
                            [name] => Savior Coffee Co
                            [location] => stdClass Object
                                (
                                    [address] => 51 Station rd
                                    [city] => Durban
                                    [country] => ZA
                                    [lat] => -29.819999117256
                                    [lng] => 31.027700659483
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [17] => stdClass Object
                        (
                            [id] => 4fba93aee4b0c8c40866ddd0
                            [name] => Marula coffee shop
                            [location] => stdClass Object
                                (
                                    [address] => 112 maryvale road westville
                                    [city] => Durban
                                    [country] => ZA
                                    [lat] => -29.853361282194
                                    [lng] => 30.998898796349
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1c8941735
                                            [name] => African Restaurant
                                            [pluralName] => African Restaurants
                                            [shortName] => African
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/african_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [18] => stdClass Object
                        (
                            [id] => 552fb56a498ed9e3d5e82361
                            [name] => Coffee Tree
                            [location] => stdClass Object
                                (
                                    [address] => 163 Lilian Ngoyeni Rd
                                    [crossStreet] => 8 Windermere Centre
                                    [city] => Durban
                                    [state] => KZN
                                    [postalCode] => 4001
                                    [country] => ZA
                                    [lat] => -29.832156
                                    [lng] => 31.02023
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [19] => stdClass Object
                        (
                            [id] => 4fffd630e4b0b5d60d3fb32a
                            [name] => Churchills Coffee Shop
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.831708401321
                                    [lng] => 31.020594634982
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [20] => stdClass Object
                        (
                            [id] => 4c4962599f2ad13adf8a3653
                            [name] => St clements coffee shop
                            [location] => stdClass Object
                                (
                                    [address] => Musgrave
                                    [crossStreet] => Musgrave road
                                    [city] => Durban
                                    [state] => KwaZulu-Natal
                                    [country] => ZA
                                    [lat] => -29.846782
                                    [lng] => 31.000353
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [21] => stdClass Object
                        (
                            [id] => 56b46e1c498e3b7e89d957f9
                            [name] => Lineage Coffee HQ
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.785916719843
                                    [lng] => 30.771813118561
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [22] => stdClass Object
                        (
                            [id] => 4fe82d89e4b0dceaf98ba29e
                            [name] => I Love Coffee
                            [location] => stdClass Object
                                (
                                    [address] => 110 Windemere Rd
                                    [country] => ZA
                                    [lat] => -29.848877330945
                                    [lng] => 30.999212444861
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [23] => stdClass Object
                        (
                            [id] => 4d0f2832ba378cfa39736f93
                            [name] => Adams Book & Coffee Shop - Musgrave
                            [location] => stdClass Object
                                (
                                    [address] => Musgrave Centre
                                    [city] => Berea
                                    [state] => KZN
                                    [country] => ZA
                                    [lat] => -29.84873758864
                                    [lng] => 30.999145234534
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d114951735
                                            [name] => Bookstore
                                            [pluralName] => Bookstores
                                            [shortName] => Bookstore
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/shops/bookstore_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06378d81259
                                            [name] => Shop & Service
                                            [pluralName] => Shops & Services
                                            [shortName] => Shops
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/shops/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [24] => stdClass Object
                        (
                            [id] => 5319a9ec498e06688dc0c82e
                            [name] => Gloria Jean's Coffees
                            [location] => stdClass Object
                                (
                                    [address] => Musgrave Centre
                                    [country] => ZA
                                    [lat] => -29.84847705439
                                    [lng] => 30.998909998096
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [25] => stdClass Object
                        (
                            [id] => 4d9877c7af3d236aeb7d3cc7
                            [name] => Gloria Jean's Coffees
                            [location] => stdClass Object
                                (
                                    [city] => Berea
                                    [state] => KZN
                                    [country] => ZA
                                    [lat] => -29.848373560508
                                    [lng] => 30.998657958547
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [26] => stdClass Object
                        (
                            [id] => 50e4ac1c8aca51909dfe618a
                            [name] => At Coffee on Florida
                            [location] => stdClass Object
                                (
                                    [address] => 204 Florida Road
                                    [city] => Durban
                                    [state] => Kwa Zulu Natal
                                    [postalCode] => 4000
                                    [country] => ZA
                                    [lat] => -29.831997946389
                                    [lng] => 31.015788316727
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [27] => stdClass Object
                        (
                            [id] => 4be19ea98dbd76b093b62091
                            [name] => The Waterberry coffee shoppe
                            [location] => stdClass Object
                                (
                                    [address] => dolphin crescent
                                    [city] => balito
                                    [state] => south africa
                                    [country] => ZA
                                    [lat] => -29.528732435516
                                    [lng] => 31.221041679382
                                )

                            [categories] => Array
                                (
                                )

                        )

                    [28] => stdClass Object
                        (
                            [id] => 53e8a9f0498ef5b49b019881
                            [name] => i love my coffee
                            [location] => stdClass Object
                                (
                                    [country] => ZA
                                    [lat] => -29.794060040084
                                    [lng] => 30.824177547644
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d16d941735
                                            [name] => Café
                                            [pluralName] => Cafés
                                            [shortName] => Café
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/cafe_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                    [29] => stdClass Object
                        (
                            [id] => 51cc4e35498ecefde5fd7181
                            [name] => Jacksonville coffee
                            [location] => stdClass Object
                                (
                                    [address] => Moses Manhida Stadium
                                    [crossStreet] => Prime Human Performance Institute
                                    [city] => Durban
                                    [state] => KwaZulu Natal
                                    [postalCode] => 4001
                                    [country] => ZA
                                    [lat] => -29.827521
                                    [lng] => 31.030972
                                )

                            [categories] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [id] => 4bf58dd8d48988d1e0931735
                                            [name] => Coffee Shop
                                            [pluralName] => Coffee Shops
                                            [shortName] => Coffee Shop
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_
                                                    [suffix] => .png
                                                )

                                        )

                                    [1] => stdClass Object
                                        (
                                            [id] => 4d4b7105d754a06374d81259
                                            [name] => Food
                                            [pluralName] => Food
                                            [shortName] => Food
                                            [icon] => stdClass Object
                                                (
                                                    [prefix] => https://ss3.4sqi.net/img/categories_v2/food/default_
                                                    [suffix] => .png
                                                )

                                        )

                                )

                        )

                )

        )

)
